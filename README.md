# SoRAIM Hands-on Prompting



## Notebook

- [SoRAIM basic prompt engineering](https://colab.research.google.com/drive/1HGtn5W_3h50ghHgvetUsRYi591KmfFSf?usp=sharing)
- [SoRAIM Retrieval Augmented Generation](https://colab.research.google.com/drive/1eFxCHSxxTEdR1c8ejfWyak9S6xE7dnNh?usp=sharing)


## Connection

WiFi SSiD : SoRAIM_winter_school
password: H2020_SPRING



### Gradio LLM web interface 
192.168.9.165:9090

user: soraim
pwd: spring
